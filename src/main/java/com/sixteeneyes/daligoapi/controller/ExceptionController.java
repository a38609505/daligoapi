package com.sixteeneyes.daligoapi.controller;

import com.sixteeneyes.daligoapi.exception.CAccessDeniedException;
import com.sixteeneyes.daligoapi.exception.CAuthenticationEntryPointException;
import com.sixteeneyes.daligoapi.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException();
    }
}
