package com.sixteeneyes.daligoapi.controller;


import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.kickboard.*;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import com.sixteeneyes.daligoapi.service.kickBoard.KickBoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Api(tags = "킥보드 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board")
public class KickBoardController {
    private final KickBoardService kickBoardService;

    @ApiOperation(value = "킥보드 등록")
    @PostMapping("/data")
    public CommonResult setKickBoard(@RequestBody @Valid KickBoardRequest request) {
        kickBoardService.setKickBoard(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "킥보드 대량 등록")
    @PostMapping("/file-upload")
    public CommonResult setKickBoards(@RequestParam("csvFile") MultipartFile csvFile) throws Exception {
        kickBoardService.setKickBoards(csvFile);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "근처 킥보드 리스트")
    @GetMapping("/near")
    public ListResult<NearKickBoardItem> getNearList(@RequestParam("posX") double posX, @RequestParam("posY") double posY, @RequestParam("distanceKm") int distanceKm) {
        return ResponseService.getListResult(kickBoardService.getNearList(posX, posY, distanceKm), true);
    }

    @ApiOperation(value = "킥보드 리스트")
    @GetMapping("/all")
    public ListResult<KickBoardItem> getList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(kickBoardService.getList(page), true);
    }

    @ApiOperation(value = "킥보드 상세 리스트")
    @GetMapping("/allall")
    public ListResult<KickBoardResponse> getData() {
        return ResponseService.getListResult(kickBoardService.getKickBoardList(), true);
    }

    @ApiOperation(value = "킥보드 수정")
    @PutMapping("/{id}")
    public CommonResult putKickBoard(@PathVariable long id, @RequestBody @Valid KickBoardUpdateRequest request) {
        kickBoardService.putKickBoard(id, request);
        return ResponseService.getSuccessResult();
    }
}
