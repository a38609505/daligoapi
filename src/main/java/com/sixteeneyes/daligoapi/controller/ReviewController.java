package com.sixteeneyes.daligoapi.controller;

import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.review.ReviewItem;
import com.sixteeneyes.daligoapi.model.review.ReviewRequest;
import com.sixteeneyes.daligoapi.service.ReviewService;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "후기 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/review")
public class ReviewController {
    private final ReviewService reviewService;

    @ApiOperation(value = "후기 등록")
    @PostMapping("/data")
    public CommonResult setReview(@RequestBody @Valid ReviewRequest request) {
        reviewService.setReview(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "후기 리스트")
    @GetMapping("/all")
    public ListResult<ReviewItem> getList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(reviewService.getList(page), true);
    }

    @ApiOperation(value = "후기 수정")
    @PutMapping("/{id}")
    public CommonResult putReview(@PathVariable long id, @RequestBody @Valid ReviewRequest request) {
        reviewService.putReview(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "후기 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delReview(@PathVariable long id) {
        reviewService.delReview(id);

        return ResponseService.getSuccessResult();
    }
}
