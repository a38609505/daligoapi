package com.sixteeneyes.daligoapi.controller;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.member.*;
import com.sixteeneyes.daligoapi.service.RemainAmountService;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import com.sixteeneyes.daligoapi.service.member.MemberDataService;
import com.sixteeneyes.daligoapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberDataService memberDataService;
    private final ProfileService profileService;
    private final RemainAmountService remainAmountService;


    @ApiOperation(value = "회원 등록")
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest memberJoinRequest) {
        Member member = memberDataService.setMember(memberJoinRequest.getMemberGroup(), memberJoinRequest);
        remainAmountService.setAmount(member);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 리스트")
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(memberDataService.getMembers(page), true);
    }

    @ApiOperation(value = "회원 수정")
    @PutMapping("/member-update")
    public CommonResult putMember(@RequestBody @Valid MemberUpdateRequest request) {
        Member member = profileService.getMemberData();
        memberDataService.putMember(member, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 가입")
    @PostMapping("/join")
    public CommonResult joinMember(@RequestBody @Valid MemberCreateRequest createRequest) {
        memberDataService.setMember(createRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "아이디 찾기")
    @PostMapping("/find-username")
    public String findUsername(@RequestBody @Valid FindUsernameRequest request) {
        return memberDataService.findUsername(request);
    }

    @ApiOperation(value = "비밀번호 수정")
    @PutMapping("/password")
    public CommonResult putPassword(@RequestBody @Valid PasswordChangeRequest changeRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putPassword(member, changeRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "비밀번호 찾기")
    @PutMapping("/find-password")
    public CommonResult findPassword(@RequestBody @Valid FindPasswordRequest request) {
        memberDataService.putPassword(request);
        return ResponseService.getSuccessResult();
    }
}
