package com.sixteeneyes.daligoapi.controller;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.model.AmountChargeRequest;
import com.sixteeneyes.daligoapi.model.AmountChargingItem;
import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.service.AmountChargeService;
import com.sixteeneyes.daligoapi.service.RemainAmountService;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import com.sixteeneyes.daligoapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "금액 충전 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/amount-charging")
public class AmountChargingController {
    private final AmountChargeService amountChargingService;
    private final ProfileService profileService;
    private final RemainAmountService remainAmountService;

    @ApiOperation(value = "금액 충전")
    @PostMapping("/data")
    public CommonResult setAmountCharging(double price) {
        Member member = profileService.getMemberData();
        amountChargingService.setAmountCharging(member, price);
        remainAmountService.putAmount(member, price);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "금액 충전 리스트")
    @GetMapping("/all")
    public ListResult<AmountChargingItem> getList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(amountChargingService.getList(page), true);
    }

    @ApiOperation(value = "나의 금액 충전 리스트")
    @GetMapping("/my-charging")
    public ListResult<AmountChargingItem> getMyList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(amountChargingService.getMyList(member, page), true);
    }
}
