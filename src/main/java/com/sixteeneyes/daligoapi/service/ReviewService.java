package com.sixteeneyes.daligoapi.service;

import com.sixteeneyes.daligoapi.entity.Review;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.review.ReviewItem;
import com.sixteeneyes.daligoapi.model.review.ReviewRequest;
import com.sixteeneyes.daligoapi.repository.ReviewRepository;
import com.sixteeneyes.daligoapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ReviewService {
    private final ReviewRepository reviewRepository;

    public void setReview(ReviewRequest request) {
        Review review = new Review.Builder(request).build();
        reviewRepository.save(review);
    }

    public ListResult<ReviewItem> getList(int page) {
        Page<Review> originList = reviewRepository.findAllByIdGreaterThanEqualOrderById(1, ListConvertService.getPageable(page));
        List<ReviewItem> result = new LinkedList<>();
        for (Review review : originList.getContent()) {
            result.add(new ReviewItem.Builder(review).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public void putReview(long id, ReviewRequest request) {
        Review review = reviewRepository.findById(id).orElseThrow();
        review.putReview(request);
        reviewRepository.save(review);
    }

    public void delReview(long id) {
        reviewRepository.deleteById(id);
    }
}
