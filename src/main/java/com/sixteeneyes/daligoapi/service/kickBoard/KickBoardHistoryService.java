package com.sixteeneyes.daligoapi.service.kickBoard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.enums.PriceBasis;
import com.sixteeneyes.daligoapi.exception.CMissingDataException;
import com.sixteeneyes.daligoapi.model.UseStaticsHistoryResponse;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardHistoryItem;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardHistoryResponse;
import com.sixteeneyes.daligoapi.model.kickboard.KickBoardUseRequest;
import com.sixteeneyes.daligoapi.repository.KickBoardHistoryRepository;
import com.sixteeneyes.daligoapi.service.common.ListConvertService;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class KickBoardHistoryService {
    private final KickBoardHistoryRepository kickBoardHistoryRepository;

    public void setUseStart(Member member, KickBoard kickBoard, KickBoardUseRequest request) {
        KickBoardHistory kickBoardHistory = new KickBoardHistory.Builder(member, kickBoard, request).build();
        kickBoardHistoryRepository.save(kickBoardHistory);
    }

    public KickBoardHistory setUseEnd(long id, KickBoardUseRequest request) {
        // 이용내역id를 기준으로 데이터를 가져온다.
        KickBoardHistory kickBoardHistory = kickBoardHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);

        // 위에서 가져온 데이터에 종료 위도, 종료 경도, 종료 시간을 넣는다.
        kickBoardHistory.putUseEnd(request);

        // 킥보드 테이블의 기준요금표를 가져온다.
        PriceBasis priceBasis = kickBoardHistory.getKickBoard().getPriceBasis();

        // 기본요금
        double basePrice = priceBasis.getBasePrice();

        // 종료시작 - 시작시간 : 초
        long totalSecond = ChronoUnit.SECONDS.between(kickBoardHistory.getDateStart(), kickBoardHistory.getDateEnd());

        // 초를 분으로 변환
        double perMin = Math.ceil(totalSecond / 60.0);

        // 분당추가요금 계산
        double addPrice = perMin * priceBasis.getPerMinPrice();

        // 최종 이용 요금
        double totalPrice = basePrice + addPrice;

        // 이용요금을 이용내역에 기록 + 이용완료로 처리
        kickBoardHistory.putEndPrices(totalPrice);

        // 이 팀은 이용내역만 관리하는 팀이라서 회원 잔액조정이나 킥보드 상태를 변경할 수 없으니
        // Member와 KickBoard 정보를 모두 담고 있는 KickBoardHistory 엔티티 전체를 넘겨 준다. (컨트롤러에서 알아서 해라 시전)
        return kickBoardHistoryRepository.save(kickBoardHistory);
    }

    public ListResult<KickBoardHistoryItem> getUseHistory(int page) {
        Page<KickBoardHistory> originList = kickBoardHistoryRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));

        List<KickBoardHistoryItem> result = new LinkedList<>();

        for (KickBoardHistory kickBoardHistory : originList) {
            result.add(new KickBoardHistoryItem.Builder(kickBoardHistory).build());
        }

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public ListResult<KickBoardHistoryItem> getMyUseHistory(Member member, int page) {
        Page<KickBoardHistory> originList = kickBoardHistoryRepository.findAllByIdGreaterThanEqualAndMemberOrderByIdDesc(1, member, ListConvertService.getPageable(page));

        List<KickBoardHistoryItem> result = new LinkedList<>();

        for (KickBoardHistory kickBoardHistory : originList) {
            result.add(new KickBoardHistoryItem.Builder(kickBoardHistory).build());
        }

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public ListResult<KickBoardHistoryResponse> getHistory(Member member, KickBoard kickBoard) {
        List<KickBoardHistory> originData = kickBoardHistoryRepository.findAll();
        List<KickBoardHistoryResponse> result = new LinkedList<>();
        originData.forEach(e -> result.add(new KickBoardHistoryResponse.Builder(member, kickBoard, e).build()));

        return ListConvertService.settingResult(result);
    }

    public UseStaticsHistoryResponse getStaticsHistoryPrice(LocalDate dateStart, LocalDate dateEnd) {
        List<KickBoardHistory> histories = kickBoardHistoryRepository.findAllByDateBaseBetween(dateStart, dateEnd);

        double totalPrice = 0;
        for (KickBoardHistory kickBoardHistory: histories) {
            totalPrice += kickBoardHistory.getResultPrice();
        }

        UseStaticsHistoryResponse response = new UseStaticsHistoryResponse();
        response.setTotalPrice(totalPrice);

        return response;
    }
}
