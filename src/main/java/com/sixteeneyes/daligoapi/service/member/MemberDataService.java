package com.sixteeneyes.daligoapi.service.member;

import com.sixteeneyes.daligoapi.entity.AuthPhone;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.enums.MemberGroup;
import com.sixteeneyes.daligoapi.enums.MessageTemplate;
import com.sixteeneyes.daligoapi.exception.*;
import com.sixteeneyes.daligoapi.lib.CommonCheck;
import com.sixteeneyes.daligoapi.model.common.ListResult;
import com.sixteeneyes.daligoapi.model.member.*;
import com.sixteeneyes.daligoapi.repository.AuthPhoneRepository;
import com.sixteeneyes.daligoapi.repository.MemberRepository;
import com.sixteeneyes.daligoapi.service.common.ListConvertService;
import com.sixteeneyes.daligoapi.service.message.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthPhoneRepository authPhoneRepository;
    private final MessageService messageService;


    public String findUsername(FindUsernameRequest request) {
        Optional<Member> member = memberRepository.findByNameAndPhoneNumber(request.getName(), request.getPhoneNumber());

        if (member.isEmpty()) throw new CMissingDataException();

        return "아이디는 " + member.get().getUsername() + " 입니다.";
    }

    public void putPassword(FindPasswordRequest request) {

        Optional<Member> member = memberRepository.findByUsernameAndNameAndPhoneNumber(request.getUsername(), request.getName(), request.getPhoneNumber());
        // 원본 가져오기

        if (member.isEmpty()) throw new CMissingDataException();
        // 만약 데이터가 없으면 던져야함.
        String newPassword = makeRandomPassword();
        Member originData = member.get();
        originData.putPassword(passwordEncoder.encode(newPassword));
        memberRepository.save(originData);
        // 비밀번호 변경 완료, 비밀번호 변경 완료했으면 문자를 보내야함.
        // 문자 보내는거는 컨트롤러에서 할 것 아님
        // 서비스를 도와주는 서비스 문자(카톡)전송 서비스 한테 부탁할 것.

        HashMap<String, String> msgMap = new HashMap<>();
        msgMap.put("임시비번", newPassword);

        messageService.sendMessage(
                MessageTemplate.TD_7777,
                "010-0000-0000",
                originData.getPhoneNumber(),
                msgMap
                // 문자 전송
        );
    }

    private String makeRandomPassword() {
        Random random = new Random(); // 랜덤 숫자 생성
        int createNum = 0; // 하나의 숫자를 만들어서 기본값 0으로 초기화 / createNum은 한 자리의 랜덤 숫자를 만듦
        String ranNum = ""; // 랜덤 숫자가 들어갈 자리 생성
        String resultNum = ""; // 결과 숫자가 들어갈 자리 생성

        for (int i=0; i<8; i++) { // int 타입으로 줘서 i=0는 초기값으로 설정하고 i<6 은 6번 돈다는 걸 의미하고 i++ 초기값으로 설정된 사이클 횟수를 늘려준다.
            // 얘가 6자리를 만들기 위해선 하나의 랜덤숫자를 6번 반복해야됨
            createNum = random.nextInt(9); // 0부터 9까지의 랜덤 숫자를 생성함 for문이니까 반복
            ranNum = Integer.toString(createNum); // 나온 int타입의 랜덤 숫자를 String 타입으로 줘서 문자로 바꿈
            // 6자리로 랜덤숫자를 만들어야 하는데 숫자끼리 더하면 6자리가 나오지 않고 문자로 바꿔야 6자리로 나열할 수 있기 때문에 int타입을 String 타입으로 변환한다. 1 + 2 = 3 / "1"+"2" = 12
            resultNum += ranNum; // 결과 값은 랜덤으로 나온 숫자 6개를 전부 더한 값이다.
        }
        return resultNum;
    }

    public void putPassword(Member member, PasswordChangeRequest changeRequest) {

        if (!passwordEncoder.matches(changeRequest.getCurrentPassword(), member.getPassword())) throw new CNotMatchPasswordException();
        // 현재 비밀번호가 맞지 않으면 던져야 함.

        if (!changeRequest.getChangePassword().equals(changeRequest.getChangePasswordRe())) throw new CNotMatchPasswordException();
        // 변경 할 비밀번호와 변경 비밀번호 확인 값이 일치하지 않으면 던져야함.

        member.putPassword(passwordEncoder.encode(changeRequest.getChangePassword()));
        memberRepository.save(member);

    }

    public ListResult<MemberItem> getMembers(int page) {
        Page<Member> originList = memberRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<MemberItem> result = new LinkedList<>();
        for (Member member : originList) {
            result.add(new MemberItem.Builder(member).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public void setFirstMember() {
        String username = "superadmin";
        String password = "idjh195233";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고관리자");
            createRequest.setPhoneNumber("010-0000-0000");
            createRequest.setLicenseNumber("00-00-000000-00");
            createRequest.setDateBirth(LocalDate.parse("1997-11-08", DateTimeFormatter.ofPattern("yyyy-MM-dd")));;

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }
    }

    public void setMember(MemberCreateRequest createRequest) {
        // setMember라는 이름이 같아도 되는 이유는 개수가 다르고 타입이 다르기 때문에 같은 이름을 사용해도 괜찮다.

        Optional<AuthPhone> authPhone = authPhoneRepository.findByPhoneNumber(createRequest.getPhoneNumber());
        // 핸드폰 번호 기준으로 인증한 데이터 원본 가져오기.

        if (authPhone.isEmpty()) throw new CNotCompleteAuthException();
        // 만약 인증 데이터가 없으면 인증 시도도 안한 것. 그러면 가입 시키면 안됨.. 그래서 던져야 함.

        if (!authPhone.get().getIsAuthComplete()) throw new CNotCompleteAuthException();
        // 만약에 인증상태가 인증 진행중이면 가입시키면 안된다. 그니까 던져야함.

        setMember(MemberGroup.ROLE_USER, createRequest);
        // 진짜 본인인증 했는지 ? 물어보는 것

        authPhoneRepository.deleteById(authPhone.get().getId());
        // 인증정보를 다 사용했으니 삭제해주는 것.
    }

    public Member setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CNotValidIdException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CNotMatchPasswordException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CDuplicateIdExistException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        return memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public void putMember(Member member, MemberUpdateRequest request) {
        member.putMember(request);
        memberRepository.save(member);
    }
}
