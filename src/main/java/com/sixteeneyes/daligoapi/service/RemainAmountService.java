package com.sixteeneyes.daligoapi.service;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.entity.RemainingAmount;
import com.sixteeneyes.daligoapi.repository.RemainingAmountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RemainAmountService {
    private final RemainingAmountRepository remainingAmountRepository;

    public void setAmount(Member member) {
        RemainingAmount remainingAmount = new RemainingAmount.Builder(member).build();
        remainingAmountRepository.save(remainingAmount);
    }

    public void putAmountMinus(Member member, double price) {
        // 회원기준으로 잔액정보를 가져온다.
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member);

        // 금액을 차감시킨다.
        remainingAmount.putPriceMinus(price);

        remainingAmountRepository.save(remainingAmount);
    }

    public void putAmount(Member member, double price) {
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member);

        remainingAmount.putAmount(price);

        remainingAmountRepository.save(remainingAmount);
    }
}
