package com.sixteeneyes.daligoapi.model;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "맴버id", required = true)
    private Member member;

    @ApiModelProperty(notes = "킥보드id", required = true)
    private KickBoard kickBoard;

    @ApiModelProperty(notes = "사용 시작 시간", required = true)
    private LocalDateTime dateStart;

    @ApiModelProperty(notes = "사용 종료 시간", required = true)
    private LocalDateTime dateEnd;

    @ApiModelProperty(notes = "차감 금액", required = true)
    private Double deductedAmount;

    private KickBoardHistoryItem(Builder builder) {
        this.id = builder.id;
        this.member = builder.member;
        this.kickBoard = builder.kickBoard;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.deductedAmount = builder.deductedAmount;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryItem> {
        private final Long id;
        private final Member member;
        private final KickBoard kickBoard;
        private final LocalDateTime dateStart;
        private final LocalDateTime dateEnd;
        private final Double deductedAmount;

        public Builder(KickBoardHistory kickBoardHistory) {
            this.id = kickBoardHistory.getId();
            this.member = kickBoardHistory.getMember();
            this.kickBoard = kickBoardHistory.getKickBoard();
            this.dateStart = kickBoardHistory.getDateStart();
            this.dateEnd = kickBoardHistory.getDateEnd();
            this.deductedAmount = kickBoardHistory.getResultPrice();
        }

        @Override
        public KickBoardHistoryItem build() {
            return new KickBoardHistoryItem(this);
        }
    }
}