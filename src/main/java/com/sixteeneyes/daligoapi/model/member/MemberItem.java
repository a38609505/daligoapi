package com.sixteeneyes.daligoapi.model.member;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "맴버 그룹", required = true)
    private String memberGroup;

    @ApiModelProperty(notes = "아이디", required = true)
    private String username;

    @ApiModelProperty(notes = "비밀 번호", required = true)
    private String password;

    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    private String phoneNumber;

    @ApiModelProperty(notes = "면허증 번호", required = true)
    private String licenseNumber;

    @ApiModelProperty(notes = "포인트", required = true)
    private Double point;

    @ApiModelProperty(notes = "가입일", required = true)
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "활성화 여부", required = true)
    private Boolean isEnabled;

    private MemberItem(Builder builder) {
        this.id = builder.id;
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
        this.name = builder.name;
        this.phoneNumber = builder.phoneNumber;
        this.licenseNumber = builder.licenseNumber;
        this.point = builder.point;
        this.dateCreate = builder.dateCreate;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String memberGroup;
        private final String username;
        private final String password;
        private final String name;
        private final String phoneNumber;
        private final String licenseNumber;
        private final Double point;
        private final LocalDateTime dateCreate;
        private final Boolean isEnabled;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberGroup = member.getMemberGroup().getName();
            this.username = member.getUsername();
            this.password = member.getPassword();
            this.name = member.getName();
            this.phoneNumber = member.getPhoneNumber();
            this.licenseNumber = member.getLicenseNumber();
            this.point = member.getPoint();
            this.dateCreate = member.getDateCreate();
            this.isEnabled = member.getIsEnabled();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
