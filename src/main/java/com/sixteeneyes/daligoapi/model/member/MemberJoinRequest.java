package com.sixteeneyes.daligoapi.model.member;

import com.sixteeneyes.daligoapi.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberJoinRequest extends MemberCreateRequest {
    @NotNull
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;
}
