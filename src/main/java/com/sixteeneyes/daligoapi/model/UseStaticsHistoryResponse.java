package com.sixteeneyes.daligoapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UseStaticsHistoryResponse {
    private Double totalPrice;
}
