package com.sixteeneyes.daligoapi.model;

import com.sixteeneyes.daligoapi.entity.AmountChargingHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AmountChargingItem {
    @ApiModelProperty(notes = "시퀀스", required = true )
    @NotNull
    private Long id;

    @ApiModelProperty(notes = "맴버", required = true)
    @NotNull
    private Long member;

    @ApiModelProperty(notes = "충전 날짜", required = true)
    @NotNull
    private LocalDateTime dateCharge;

    @ApiModelProperty(notes = "가격", required = true)
    @NotNull
    private Double price;

    private AmountChargingItem(Builder builder) {
        this.id = builder.id;
        this.member = builder.member;
        this.dateCharge = builder.dateCharge;
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<AmountChargingItem> {
        private final Long id;
        private final Long member;
        private final LocalDateTime dateCharge;
        private final Double price;

        public Builder(AmountChargingHistory amountChargingHistory) {
            this.id = amountChargingHistory.getId();
            this.member = amountChargingHistory.getMember().getId();
            this.dateCharge = amountChargingHistory.getDateCharge();
            this.price = amountChargingHistory.getPrice();
        }

        @Override
        public AmountChargingItem build() {
            return new AmountChargingItem(this);
        }
    }
}
