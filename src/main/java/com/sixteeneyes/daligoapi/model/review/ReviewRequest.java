package com.sixteeneyes.daligoapi.model.review;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ReviewRequest {
    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "제목", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String title;

    @ApiModelProperty(notes = "내용", required = true)
    @NotNull
    @Length(min = 10)
    private String content;

    @ApiModelProperty(notes = "이미지주소", required = true)
    @NotNull
    private String imgName;
}
