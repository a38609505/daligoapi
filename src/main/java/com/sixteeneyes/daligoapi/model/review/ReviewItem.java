package com.sixteeneyes.daligoapi.model.review;

import com.sixteeneyes.daligoapi.entity.Review;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReviewItem {
    private Long id;
    private String name;
    private String title;
    private String content;
    private String imgName;
    private LocalDateTime dateWrite;

    private ReviewItem(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.title = builder.title;
        this.content = builder.content;
        this.imgName = builder.imgName;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<ReviewItem> {
        private final Long id;
        private final String name;
        private final String title;
        private final String content;
        private final String imgName;
        private final LocalDateTime dateWrite;

        public Builder(Review review) {
            this.id = review.getId();
            this.name = review.getName().charAt(0) + "0".repeat(review.getName().length() - 1);
            this.title = review.getTitle();
            this.content = review.getContent();
            this.imgName = review.getImgName();
            this.dateWrite = review.getDateWrite();
        }

        @Override
        public ReviewItem build() {
            return new ReviewItem(this);
        }
    }
}
