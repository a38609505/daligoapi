package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryResponse {
    @ApiModelProperty(notes = "회원id", required = true)
    private Member member;

    @ApiModelProperty(notes = "킥보드id", required = true)
    private KickBoard kickBoard;

    @ApiModelProperty(notes = "시작 위도", required = true)
    private Double startPosX;

    @ApiModelProperty(notes = "시작 경도", required = true)
    private Double startPosY;

    @ApiModelProperty(notes = "시작일", required = true)
    private LocalDateTime dateStart;

    @ApiModelProperty(notes = "종료 위도", required = true)
    private Double endPosX;

    @ApiModelProperty(notes = "종료 경도", required = true)
    private Double endPosY;

    @ApiModelProperty(notes = "기준일", required = true)
    private LocalDate dateBase;

    @ApiModelProperty(notes = "종료일", required = true)
    private LocalDateTime dateEnd;

    @ApiModelProperty(notes = "이용금액", required = true)
    private Double resultPrice;

    @ApiModelProperty(notes = "완료여부", required = true)
    private Boolean isComplete;

    private KickBoardHistoryResponse(Builder builder) {
        this.member = builder.member;
        this.kickBoard = builder.kickBoard;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
        this.endPosX = builder.endPosX;
        this.endPosY = builder.endPosY;
        this.dateBase = builder.dateBase;
        this.dateEnd = builder.dateEnd;
        this.resultPrice = builder.resultPrice;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryResponse> {

        private final Member member;
        private final KickBoard kickBoard;
        private final Double startPosX;
        private final Double startPosY;
        private final LocalDateTime dateStart;
        private final Double endPosX;
        private final Double endPosY;
        private final LocalDate dateBase;
        private final LocalDateTime dateEnd;
        private final Double resultPrice;
        private final Boolean isComplete;

        public Builder(Member member, KickBoard kickBoard, KickBoardHistory kickBoardHistory) {
            this.member = member;
            this.kickBoard = kickBoard;
            this.startPosX = kickBoardHistory.getStartPosX();
            this.startPosY = kickBoardHistory.getStartPosY();
            this.dateStart = kickBoardHistory.getDateStart();
            this.endPosX = kickBoardHistory.getEndPosX();
            this.endPosY = kickBoardHistory.getEndPosY();
            this.dateBase = kickBoardHistory.getDateBase();
            this.dateEnd = kickBoardHistory.getDateEnd();
            this.resultPrice = kickBoardHistory.getResultPrice();
            this.isComplete = kickBoardHistory.getIsComplete();
        }
        @Override
        public KickBoardHistoryResponse build() {
            return new KickBoardHistoryResponse(this);
        }
    }
}
