package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardItem {
    private Long id;
    private KickBoardStatus kickBoardStatus;
    private String uniqueNumber;

    private KickBoardItem(Builder builder) {
        this.id = builder.id;
        this.kickBoardStatus = builder.kickBoardStatus;
        this.uniqueNumber = builder.uniqueNumber;
    }

    public static class Builder implements CommonModelBuilder<KickBoardItem> {
        private final Long id;
        private final KickBoardStatus kickBoardStatus;
        private final String uniqueNumber;

        public Builder(KickBoard kickBoard) {
            this.id = kickBoard.getId();
            this.kickBoardStatus = kickBoard.getKickBoardStatus();
            this.uniqueNumber = kickBoard.getUniqueNumber();
        }

        @Override
        public KickBoardItem build() {
            return new KickBoardItem(this);
        }
    }
}
