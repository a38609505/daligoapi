package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.enums.PriceBasis;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NearKickBoardItem {
    private String kickBoardStatusName;
    private String modelName;
    private String priceBasisName;
    private Double posX;
    private Double posY;
    private Double distanceM;

    private NearKickBoardItem(Builder builder) {
        this.kickBoardStatusName = builder.kickBoardStatusName;
        this.modelName = builder.modelName;
        this.priceBasisName = builder.priceBasisName;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.distanceM = builder.distanceM;
    }

    public static class Builder implements CommonModelBuilder<NearKickBoardItem> {
        private final String kickBoardStatusName;
        private final String modelName;
        private final String priceBasisName;
        private final Double posX;
        private final Double posY;
        private final Double distanceM;

        public Builder(KickBoardStatus kickBoardStatus, String modelName, PriceBasis priceBasis, double posX, double posY, double distanceM) {
            this.kickBoardStatusName = kickBoardStatus.getName();
            this.modelName = modelName;
            this.priceBasisName = priceBasis.getName();
            this.posX = posX;
            this.posY = posY;
            this.distanceM = distanceM;
        }

        @Override
        public NearKickBoardItem build() {
            return new NearKickBoardItem(this);
        }
    }
}
