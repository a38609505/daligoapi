package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    @NotNull
    private Long id;

    @ApiModelProperty(notes = "시작일", required = true)
    @NotNull
    private LocalDateTime dateStart;

    @ApiModelProperty(notes = "종료일", required = true)
    private LocalDateTime dateEnd;


    private KickBoardHistoryItem(Builder builder) {
        this.id = builder.id;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryItem> {
        private final Long id;
        private final LocalDateTime dateStart;
        private final LocalDateTime dateEnd;

        public Builder(KickBoardHistory kickBoardHistory) {
            this.id = kickBoardHistory.getId();
            this.dateStart = kickBoardHistory.getDateStart();
            this.dateEnd = kickBoardHistory.getDateEnd();
        }

        @Override
        public KickBoardHistoryItem build() {
            return new KickBoardHistoryItem(this);
        }
    }
}
