package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardResponse {

    @ApiModelProperty(notes = "킥보드 상태", required = true)
    private KickBoardStatus kickBoardStatus;

    @ApiModelProperty(notes = "구입일", required = true)
    private LocalDate dateBuy;

    @ApiModelProperty(notes = "위도", required = true)
    private Double posX;

    @ApiModelProperty(notes = "경도", required = true)
    private Double posY;

    private KickBoardResponse(Builder builder) {
        this.kickBoardStatus = builder.kickBoardStatus;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
    }

    public static class Builder implements CommonModelBuilder<KickBoardResponse> {
        private final KickBoardStatus kickBoardStatus;
        private final LocalDate dateBuy;
        private final Double posX;
        private final Double posY;

        public Builder(KickBoard kickBoard) {
            this.kickBoardStatus = kickBoard.getKickBoardStatus();
            this.dateBuy = kickBoard.getDateBuy();
            this.posX = kickBoard.getPosX();
            this.posY = kickBoard.getPosY();
        }

        @Override
        public KickBoardResponse build() {
            return new KickBoardResponse(this);
        }
    }
}
