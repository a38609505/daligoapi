package com.sixteeneyes.daligoapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AmountChargeRequest {
    @ApiModelProperty(notes = "충전금액", required = true)
    @NotNull
    private Double price;
}
