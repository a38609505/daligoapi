package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.model.review.ReviewRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Review {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 이름
    @Column(nullable = false, length = 20)
    private String name;

    // 제목
    @Column(nullable = false, length = 30)
    private String title;

    // 내용
    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    // 이미지주소
    @Column(nullable = false)
    private String imgName;

    // 글쓴시간
    @Column(nullable = false)
    private LocalDateTime dateWrite;

    public void putReview(ReviewRequest request) {
        this.name = request.getName();
        this.content = request.getContent();
        this.title = request.getTitle();
        this.imgName = request.getImgName();
        this.dateWrite = LocalDateTime.now();
    }

    private Review(Builder builder) {
        this.name = builder.name;
        this.content = builder.content;
        this.title = builder.title;
        this.imgName = builder.imgName;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<Review> {
        private final String name;
        private final String content;
        private final String title;
        private final String imgName;
        private final LocalDateTime dateWrite;

        public Builder(ReviewRequest request) {
            this.name = request.getName();
            this.content = request.getContent();
            this.title = request.getTitle();
            this.imgName = request.getImgName();
            this.dateWrite = LocalDateTime.now();
        }

        @Override
        public Review build() {
            return new Review(this);
        }
    }
}
