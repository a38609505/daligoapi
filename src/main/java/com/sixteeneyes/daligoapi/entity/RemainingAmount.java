package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RemainingAmount {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    // 잔여금액
    @Column(nullable = false)
    private Double remainPrice;

    public void putAmount(double price) {
        this.remainPrice = this.remainPrice + price;
    }

    private RemainingAmount(Builder builder) {
        this.member = builder.member;
        this.remainPrice = builder.remainPrice;
    }

    public void putPriceMinus(double price) {
        this.remainPrice -= price;
    }

    public static class Builder implements CommonModelBuilder<RemainingAmount> {
        private final Member member;
        private final Double remainPrice;

        public Builder(Member member) {
            this.member = member;
            this.remainPrice = 0D;
        }

        @Override
        public RemainingAmount build() {
            return new RemainingAmount(this);
        }
    }
}
