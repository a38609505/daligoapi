package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.entity.Member.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface KickBoardHistoryRepository extends JpaRepository<KickBoardHistory, Long> {
    Page<KickBoardHistory> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
    Page<KickBoardHistory> findAllByIdGreaterThanEqualAndMemberOrderByIdDesc(long id, Member member, Pageable pageable);
    List<KickBoardHistory> findAllByDateBaseBetween(LocalDate dateStart, LocalDate dateEnd);
}
