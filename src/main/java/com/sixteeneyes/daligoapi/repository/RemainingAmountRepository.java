package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.entity.RemainingAmount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RemainingAmountRepository extends JpaRepository<RemainingAmount, Long> {
    RemainingAmount findByMember(Member member);
}
