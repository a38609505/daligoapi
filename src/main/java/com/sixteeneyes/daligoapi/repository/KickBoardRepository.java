package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KickBoardRepository extends JpaRepository<KickBoard, Long> {
    Page<KickBoard> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
