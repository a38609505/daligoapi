package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.AuthPhone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthPhoneRepository extends JpaRepository<AuthPhone, Long> {
    Optional<AuthPhone> findByPhoneNumber(String phoneNumber);
}
