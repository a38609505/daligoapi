package com.sixteeneyes.daligoapi.advice;

import com.sixteeneyes.daligoapi.enums.ResultCode;
import com.sixteeneyes.daligoapi.exception.*;
import com.sixteeneyes.daligoapi.model.common.CommonResult;
import com.sixteeneyes.daligoapi.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CNotValidIdException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotValidIdException e) {
        return ResponseService.getFailResult(ResultCode.NOT_VALID_ID);
    }

    @ExceptionHandler(CNotMatchPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotMatchPasswordException e) {
        return ResponseService.getFailResult(ResultCode.NOT_MATCH_PASSWORD);
    }

    @ExceptionHandler(CDuplicateIdExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDuplicateIdExistException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATE_ID_EXIST);
    }

    @ExceptionHandler(CNotCompleteAuthException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotCompleteAuthException e) {
        return ResponseService.getFailResult(ResultCode.NOT_COMPLETE_AUTH);
    }

    @ExceptionHandler(CNotMatchAuthException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotMatchAuthException e) {
        return ResponseService.getFailResult(ResultCode.NOT_MATCH_AUTH);
    }

    @ExceptionHandler(CAlreadyExistAuthException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CAlreadyExistAuthException e) {
        return ResponseService.getFailResult(ResultCode.ALREADY_EXIST_AUTH);
    }
}
